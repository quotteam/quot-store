const express = require('express');
const app = express();
const Controller = require('./ytimg/Controller');

app.use('/ytimg', Controller);
app.use('/thumb', Controller);

module.exports = app;
