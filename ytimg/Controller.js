const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const {fork} = require('child_process');

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

router.get('/:youtubeId/:moment', function (req, res) {

  const youtubeId = req.params.youtubeId;
  const momentSecond = Number(req.params.moment);

  if (!youtubeId || typeof youtubeId !== 'string' ||
        youtubeId.length === 0 || momentSecond < 0) {
    res.status(500).send({
      msg: 'Invalid input params'
    });
    return true;
  }

  const compute = fork('./ytimg/Thumbnail.js', [`--youtubeId=${youtubeId}`, `--momentSecond=${momentSecond}`]);
  compute.send('start');
  compute.on('message', thumbName => {
    res.status(200).send({
      thumb: thumbName
    });
  });
});

router.get('/:thumbName', function (req, res, next) {

  const thumbName = req.params.thumbName;

  if (!thumbName || typeof thumbName !== 'string' || thumbName.length === 0) {
    res.status(500).send({
      msg: 'Invalid thumb name'
    });
    return true;
  }

  const options = {
    root: `${process.env.PWD}/thumb/`,
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  };

  res.sendFile(thumbName, options, function (err) {
    if (err) {
      res.status(404).send({
        msg: 'Thumb not found'
      });
      next(err);
    }
  });
});

module.exports = router;
