const fs = require('fs');
const ytdl = require('ytdl-core');
const {exec} = require('child_process');
const argv = require('yargs').argv;

const buildThumbnailFFmpegCommand = (momentSecond, videoSrc, thumbPath) => {
  return `ffmpeg -ss ${momentSecond} -i "${videoSrc}" -f image2 -vframes 1 -y ${thumbPath}`;
};

const saveThumbnail = (youtubeId, momentSecond) => {

  const thumbName = `${youtubeId}-${momentSecond}.jpg`;

  ytdl.getInfo(youtubeId, (error, result) => {
    if (error) {
      console.log(error);
    }
    else {
      const format = ytdl.chooseFormat(result.formats, {
        filter: 'video'
      });
      const videoSrc = format.url;
      const thumbPath = `${process.env.PWD}/thumb/`;

      if (!fs.existsSync(thumbPath)) {
        fs.mkdirSync(thumbPath);
      }

      const outputThumbPath = thumbPath + thumbName;

      exec(buildThumbnailFFmpegCommand(momentSecond, videoSrc, outputThumbPath), (err) => {
        if (err || !fs.existsSync(outputThumbPath)) {
          console.log(err);
        }
      });
    }
  });

  return thumbName;
};

process.on('message', (msg) => {
  const thumbName = saveThumbnail(argv.youtubeId, argv.momentSecond);
  process.send(thumbName);
});
