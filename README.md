# Quot-store

### Installation

Install locally for development

```sh
$ npm i
$ npm start
```

### Building with docker

```sh
$ docker build --tag=quot_store .
$ docker run -p 4000:3000 -d quot_store
```

### Run on production with PM2

```sh
$ npm install
$ npm run start-production
```

### API

**GET /ytimg/youtube_id/moment - request for generating**
- youtube_id - youtube video id., e.g nNa1wbCuH0g
- moment -  moment in seconds., e.g 10

| Code | Response | About |
| ------ | ------ | ------ |
| 500 | {msg: 'Invalid input params'} | FFmpeg return error |
| 200 | {"thumb":"thumb/nNa1wbCuH0g-1.png"} | Thumb path generated, generator start work


**GET /thumb/thumbName - get generated thumb**
- thumbName - youtube thumb name., e.g nNa1wbCuH0g-1.png

| Code | Response | About |
| ------ | ------ | ------ |
| 404 | {"msg":"Thumb not found"} | Invalid thumbName |
| 200 | image | Render image
| 500 | {msg: 'Invalid thumb name'} | FFmpeg return error 
